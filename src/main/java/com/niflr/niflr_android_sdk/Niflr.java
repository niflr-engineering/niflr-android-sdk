package com.niflr.niflr_android_sdk;

import org.json.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;
import android.util.Log;

public class Niflr {
    private WebSocketClient socket;

    HashMap<String, ArrayList> subscribers = new HashMap<>();


    public void subscribe(String eventName, Runnable callback) {
        if (!subscribers.containsKey(eventName)) {
            subscribers.put(eventName, new ArrayList());
        }
        ArrayList arrayVal = subscribers.get(eventName);
        arrayVal.add(callback);
    }

    private void publish(String eventName) {
        if (!subscribers.containsKey(eventName)) {
            return;
        }
        ArrayList arrayVal = subscribers.get(eventName);
        for (int i = 0; i < arrayVal.size(); i++ ) {
            Runnable cb = (Runnable) arrayVal.get(i);
            cb.run();
        }
    }

    public void printSubscribers(){
        System.out.println("subscribers");
    }

    public void init(final HashMap config) {
        URI uri;
        try {
            uri = new URI("ws://192.168.1.7:8080/");
        } catch (URISyntaxException e) {
            e.printStackTrace();
            return;
        }

        socket = new WebSocketClient(uri) {
            @Override
            public void onOpen(ServerHandshake serverHandshake) {
                Log.i("Websocket", "Opened");
                socket.send(new JSONObject(config).toString());
            }

            @Override
            public void onMessage(String message) {
                try {
                    JSONObject obj = new JSONObject(message);
                    publish(obj.get("eventName").toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onClose(int i, String s, boolean b) {
                Log.i("Websocket", "Closed " + s);
            }

            @Override
            public void onError(Exception e) {
                Log.i("Websocket", "Error " + e.getMessage());
            }
        };
        socket.connect();
    }
}
