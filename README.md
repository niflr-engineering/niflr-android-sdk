# Niflr Android SDK #

This README would normally document whatever steps are necessary to get your application up and running.

### Your Main Application ###

* Add following permissions to `AndroidManifest.xml` 
	* <uses-permission android:name="android.permission.INTERNET"/>
    * <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
    * <uses-permission android:name="android.permission.READ_PHONE_STATE" />

------------------------------------

Sample Code of `MainActivity.java`

``


```java
package com.niflr.myapplication;

import org.json.*;
import com.niflr.niflr_android_sdk.Niflr;
.....

public class MainActivity extends AppCompatActivity {
    Runnable callback1 = new Runnable() {
        @Override
        public void run() {
            System.out.println("Cart Update");
            Snackbar.make(findViewById(R.id.fab), "Cart Update - callback called", Snackbar.LENGTH_LONG).show();
        }
    };

    Runnable callback2 = new Runnable() {
        @Override
        public void run() {
            System.out.println("Cart Checkout");
            Snackbar.make(findViewById(R.id.fab), "Cart Checkout - callback called", Snackbar.LENGTH_LONG).show();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ....
        
        Niflr niflr = new Niflr();
        HashMap<String, String> config = new HashMap<>();

        config.put("type", "authenticate");
        config.put("key", "3668a38b-d25d-47ee-8da2-19a36d51e3da");
        config.put("secret", "6bef18936ac12a9096e9fe7a8fe1f777");

        /* initialize niflr websocket */
        niflr.init(config);

        try {
            niflr.subscribe("cart_updated", callback1);
            niflr.subscribe("cart_checkout", callback2);

            niflr.printSubscribers();
        } catch (Exception e) {
            e.printStackTrace();
        }

        setContentView(R.layout.activity_main);
        .....
    }
    .....
}
```
